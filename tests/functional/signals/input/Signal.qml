Item {
    function move(x, y);

    signal moved(int x, int y);
    signal started(xChord: int, yChord: int)

    function doSomething();
}
